package com.viachat.gds.test;

import java.io.IOException;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.PrivateKey;

import javax.security.auth.PrivateCredentialPermission;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.tools.remoteapi.RemoteApiInstaller;
import com.google.appengine.tools.remoteapi.RemoteApiOptions;

public class RemoteApiExample {
	public static void main(String[] args) throws IOException {
		/*
		 * String username = System.console().readLine("username: "); String
		 * password = new String(System.console() .readPassword("password: "));
		 */
//		RemoteApiOptions options = new RemoteApiOptions().server(
//		"boreal-axiom-116609.appspot.com", 443).useServiceAccountCredential("viachatgds@boreal-axiom-116609.iam.gserviceaccount.com", "/home/shirshendu/Documents/GoogleDatastoreExample-32ed189259eb.p12");
		RemoteApiOptions options = new RemoteApiOptions().server(
				"boreal-axiom-116609.appspot.com", 443)
				.useApplicationDefaultCredential();
		RemoteApiInstaller installer = new RemoteApiInstaller();
		installer.install(options);
		try {
			DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
			System.out.println("Key of new entity is "
					+ ds.put(new Entity("Hello Remote API!")));
		} finally {
			installer.uninstall();
		}
	}
}
