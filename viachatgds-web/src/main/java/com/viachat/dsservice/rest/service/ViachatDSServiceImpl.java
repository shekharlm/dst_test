package com.viachat.dsservice.rest.service;

import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.communication.message.AuthorInfoDTO;
import com.communication.message.ComponentMessageDTO;
import com.communication.message.IncExcListDTO;
import com.communication.message.RecipientDetailsDTO;
import com.communication.message.TimestampDTO;
import com.viachat.dsservice.api.IDatastoreAPI;
import com.viachat.dsservice.dao.DatastorePersistence;
import com.viachat.dsservice.entity.ComponentMessage;
import com.viachat.dsservice.exception.DSException;

/**
 * @author naveen
 *
 */
@Path("/v2")
public class ViachatDSServiceImpl {

	
	private static IDatastoreAPI persistence;

	private static Logger LOGGER = Logger.getLogger(ViachatDSServiceImpl.class
			.getName());

	static{
		persistence = new DatastorePersistence();
	}
	
	@Path("/create/componentMessage")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response storeComponentMessage(ComponentMessage cmpMsgDTO) {
		
		if(cmpMsgDTO == null) return Response.status(Response.Status.EXPECTATION_FAILED).entity("FAILED").build();
		
		try {
			persistence.storeComponentMessage(cmpMsgDTO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return Response.status(Response.Status.OK).entity("SUCCESS")
				.build();
	}
	
	@Path("/get/componentMessage/recipientDetails")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getComponentMessageByRecipientDetails(RecipientDetailsDTO recpDetails) {
		
		List<ComponentMessage> cmpMsgResList = null;
		try {
			cmpMsgResList = persistence.getComponentMessageByRecipentDetails(recpDetails);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return Response.status(Response.Status.OK).entity(cmpMsgResList)
				.build();
	}
	
	@Path("/get/componentMessage")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getComponentMessageByInTimestamp(TimestampDTO cmpMsgDTO) {
		
		List<ComponentMessage> cmpMsgResList= null;
		
		try {
			cmpMsgResList= persistence.getComponentMessageByInTimestamp(cmpMsgDTO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return Response.status(Response.Status.OK).entity(cmpMsgResList)
				.build();
	}
	
	@Path("/get/componentMessage/author/")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getComponentMessageByAuthorSid(AuthorInfoDTO authorInfo) {
		
		List<ComponentMessage> cmpMsgResList= null;
		
		try {
			cmpMsgResList= persistence.getComponentMessageByAuthorInfo(authorInfo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return Response.status(Response.Status.OK).entity(cmpMsgResList)
				.build();
	}
	
	@Path("/get/componentMessage/list")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getComponentMessageByIncExcList(IncExcListDTO incExcListDTO) {
		
		List<ComponentMessage> cmpMsgResList= null;
		
		try {
			cmpMsgResList= persistence.getComponentMessageByIncExcList(incExcListDTO);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return Response.status(Response.Status.OK).entity(cmpMsgResList)
				.build();
	}
	
}
