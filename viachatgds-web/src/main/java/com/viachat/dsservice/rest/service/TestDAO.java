package com.viachat.dsservice.rest.service;

import java.io.Serializable;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include=Inclusion.NON_NULL)
public class TestDAO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9190394610801733744L;
	private String name;
	private String classy;
	public String getClassy() {
		return classy;
	}
	public void setClassy(String classy) {
		this.classy = classy;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public TestDAO() {
		// TODO Auto-generated constructor stub
	}
}
