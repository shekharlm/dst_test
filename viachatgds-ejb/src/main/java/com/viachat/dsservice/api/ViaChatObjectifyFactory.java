package com.viachat.dsservice.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.appengine.api.datastore.AsyncDatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceConfig;
import com.google.appengine.tools.remoteapi.RemoteApiOptions;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.remotely.RemoteCheck;
import com.googlecode.objectify.remotely.Remotely;
import com.viachat.dsservice.entity.ComponentMessage;

public class ViaChatObjectifyFactory extends ObjectifyFactory {
	private final Remotely remotely;
	
	Logger logger=LoggerFactory.getLogger(ViaChatObjectifyFactory.class);
	
	public ViaChatObjectifyFactory(){
		register(ComponentMessage.class);
		
		logger.info("Cretating remote connections");
		RemoteApiOptions options=new RemoteApiOptions().server(
				"boreal-axiom-116609.appspot.com", 443)
				.useApplicationDefaultCredential();
		
		remotely=new Remotely(options, new RemoteCheck() {
			public boolean isRemote(String namespace) {
				return true;
			}
		});
		
	}
	
	@Override
	protected AsyncDatastoreService createRawAsyncDatastoreService(DatastoreServiceConfig cfg) {
		logger.info("Returning DS Service");
		return remotely.intercept(super.createRawAsyncDatastoreService(cfg));
	}

}
