package com.viachat.dsservice.api;

import java.util.List;

import com.communication.message.AuthorInfoDTO;
import com.communication.message.ComponentMessageDTO;
import com.communication.message.IncExcListDTO;
import com.communication.message.RecipientDetailsDTO;
import com.communication.message.TimestampDTO;
import com.viachat.dsservice.entity.ComponentMessage;
import com.viachat.dsservice.exception.DSException;

public interface IDatastoreAPI {
	/**
	 * @param msg
	 * @throws DSException
	 * @throws Exception
	 */
	public void storeComponentMessage(ComponentMessage msg) throws DSException,
			Exception;

	/**
	 * @param recpDetails
	 * @return
	 * @throws DSException
	 * @throws Exception
	 */
	public List<ComponentMessage> getComponentMessageByRecipentDetails(
			RecipientDetailsDTO recpDetails) throws DSException, Exception;

	/**
	 * @param cmpMsgDTO
	 * @return
	 * @throws DSException
	 * @throws Exception
	 */
	public List<ComponentMessage> getComponentMessageByInTimestamp(
			TimestampDTO cmpMsgDTO) throws DSException, Exception;

	/**
	 * @param authorInfo
	 * @return
	 * @throws DSException
	 * @throws Exception
	 */
	public List<ComponentMessage> getComponentMessageByAuthorInfo(
			AuthorInfoDTO authorInfo) throws DSException, Exception;

	/**
	 * @param incExcListDTO
	 * @return
	 * @throws DSException
	 * @throws Exception
	 */
	public List<ComponentMessage> getComponentMessageByIncExcList(
			IncExcListDTO incExcListDTO) throws DSException, Exception;
}
