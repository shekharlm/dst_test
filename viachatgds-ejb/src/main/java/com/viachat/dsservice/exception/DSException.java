package com.viachat.dsservice.exception;

public class DSException extends Exception {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8092291178323923184L;
	/**
	 * 
	 */
	private String errorCode;
	private String errorMessage;
	private String developerMessage;
	
	public DSException(String errorCode, String errorMessage, String developerMessage) {
		super(errorCode);
		this.errorCode=errorCode;		
		this.errorMessage=errorMessage;
		this.developerMessage=developerMessage;
	}
	
	public DSException(String errorCode,Throwable cause){
		super(errorCode,cause);
	}
	
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getDeveloperMessage() {
		return developerMessage;
	}
	public void setDeveloperMessage(String developerMessage) {
		this.developerMessage = developerMessage;
	}
	
	@Override
	public String getMessage(){
		return this.errorCode;
	}
		
	@Override
	public String toString() {
		return new StringBuilder( "DSException [errorCode=" + errorCode
				+ ", errorMessage=" + errorMessage + ", developerMessage="
				+ developerMessage + "]").toString();

	}

}
