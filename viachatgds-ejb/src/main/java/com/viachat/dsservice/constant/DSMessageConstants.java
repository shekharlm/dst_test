package com.viachat.dsservice.constant;

public class DSMessageConstants {
	
	public static final String RECIPIENT_SID= "DS_VC_001"; //Recipient sid should not be null
	public static final String RECIPIENT_TYPE = "DS_VC_002"; //Recipient type should not be null"
	public static final String AUTHOR_SID = "DS_VC_003"; //Author sid should not be null
	public static final String TIMESTAMP = "DS_VC_004"; //Timestamp should not be null
	public static final String COMPONENT_MSG_TYPE = "DS_VC_005"; //component msg type should not be null
	public static final String NO_ENTITY_FOUND = "DS_VC_006"; //No entity found
}
