package com.viachat.dsservice.dao;

import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.communication.message.AuthorInfoDTO;
import com.communication.message.IncExcListDTO;
import com.communication.message.RecipientDetailsDTO;
import com.communication.message.TimestampDTO;
import com.dozer.mapper.DozerUtil;
import com.googlecode.objectify.ObjectifyService;
import com.viachat.dsservice.api.IDatastoreAPI;
import com.viachat.dsservice.api.ViaChatObjectifyFactory;
import com.viachat.dsservice.entity.ComponentMessage;
import com.viachat.dsservice.exception.DSException;

/**
 * @author naveen
 *
 */
public class DatastorePersistence implements IDatastoreAPI {

	// registering entity classes
	static {
		ObjectifyService.setFactory(new ViaChatObjectifyFactory());
//		ObjectifyService.register(ComponentMessage.class);
	}

	@Inject
	DozerUtil dozer;

	private static final Logger logger = LoggerFactory
			.getLogger(DatastorePersistence.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.viachat.dsservice.api.IDatastoreAPI#storeComponentMessage(com.viachat
	 * .dsservice.entity.ComponentMessage)
	 */
	public void storeComponentMessage(ComponentMessage msg) throws DSException,
			Exception {
		logger.trace("Inside DatastorePersistence :: storeComponentMessage()");
		// TODO Dozer conversion from ComponentMessage TO entity
		try {
			// cmpMsg = dozer.convert(msg, ComponentMessage.class);
			if (msg == null) {
				logger.info("While saving entity :: Entity object is null ");
				throw new Exception("Entity object is null {} ");
			}
			ObjectifyService.ofy().save().entity(msg).now();
		} catch (Exception e) {
			logger.error("Error while saving entity {} ", e.getMessage());
			throw new Exception("Error while saving entity {} "
					+ e.getMessage());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.viachat.dsservice.api.IDatastoreAPI#getComponentMessageByRecipentDetails
	 * (com.communication.message.RecipientDetailsDTO)
	 */
	public List<ComponentMessage> getComponentMessageByRecipentDetails(
			RecipientDetailsDTO recpDetails) throws DSException, Exception {
		logger.trace("Inside DatastorePersistence :: getComponentMessageByRecipentDetails()");

		if (recpDetails.getRecipientSid() == null
				&& recpDetails.getRecipientSid().trim() == "") {
			logger.info("While fetching  entity by recipient details:: Recipient sid cannot be empty ");
			throw new Exception("Recipient sid cannot be empty");
		} else if (recpDetails.getRecipientType() == null) {
			logger.info("While fetching  entity by recipient details:: Recipient type cannot be null ");
			throw new Exception("Recipient type cannot be empty");
		}
		List<ComponentMessage> cmpMsg = null;
		try {
			// fetching entity component message by recipient details
			cmpMsg = ObjectifyService
					.ofy()
					.load()
					.type(ComponentMessage.class)
					.filter("recipientDetails.recipientSid =",
							recpDetails.getRecipientSid())
					.filter("recipientDetails.recipientType =",
							recpDetails.getRecipientType()).list();

		} catch (Exception e) {
			logger.error(
					"Error while fetching  entity by recipient details {} ",
					e.getMessage());
			throw new Exception(
					"Error while fetching  entity by recipient details {} "
							+ e.getMessage());
		}
		return cmpMsg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.viachat.dsservice.api.IDatastoreAPI#getComponentMessageByInTimestamp
	 * (com.communication.message.TimestampDTO)
	 */
	public List<ComponentMessage> getComponentMessageByInTimestamp(
			TimestampDTO cmpMsgDTO) throws DSException, Exception {
		logger.trace("Inside DatastorePersistence :: getComponentMessageByInTimestamp()");
		List<ComponentMessage> cmpMsg = null;
		if (cmpMsgDTO.getSid() == null && cmpMsgDTO.getSid() == "") {
			logger.info("While fetching  entity by recipient sid and timestamp :: Recipient sid cannot be empty ");
			throw new Exception("Recipient sid cannot be empty");
		} else if (cmpMsgDTO.getTimestamp() == null
				|| cmpMsgDTO.getTimestamp2() == null) {
			logger.info("While fetching  entity by recipient sid and timestamp :: Timestamp cannot be null ");
			throw new Exception("Timestamp cannot be null");
		}
		try {
			cmpMsg = ObjectifyService
					.ofy()
					.load()
					.type(ComponentMessage.class)
					//.filter("recipientDetails.recipientSid =",
						//	cmpMsgDTO.getSid())
					// .filter("expiresOn =", null)
					.filter("timestamp >", cmpMsgDTO.getTimestamp())
					.filter("timestamp <", cmpMsgDTO.getTimestamp2()).list();

		} catch (Exception e) {
			logger.error(
					"Error while fetching  entity by recipient sid and timestamp {} ",
					e.getMessage());
			throw new Exception(
					"Error while fetching  entity by recipient sid and timestamp {} "
							+ e.getMessage());
		}
		return cmpMsg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.viachat.dsservice.api.IDatastoreAPI#getComponentMessageByAuthorInfo
	 * (com.communication.message.AuthorInfoDTO)
	 */
	public List<ComponentMessage> getComponentMessageByAuthorInfo(
			AuthorInfoDTO authorInfo) throws DSException, Exception {
		logger.trace("Inside DatastorePersistence :: getComponentMessageByAuthorInfo()");
		if (authorInfo.getSid() == null && authorInfo.getSid() == "") {
			logger.info("While fetching  entity by author sid :: AuthorInfo sid cannot be empty ");
			throw new Exception("AuthorInfo sid cannot be empty");
		}

		List<ComponentMessage> cmpMsg = null;

		try {
			cmpMsg = ObjectifyService.ofy().load().type(ComponentMessage.class)
					.filter("authorInfo.sid =", authorInfo.getSid()).list();

		} catch (Exception e) {
			logger.error("Error while fetching  entity by authorinfo sid",
					e.getMessage());
			throw new Exception(
					"Error while fetching  entity by authorinfo sid {} "
							+ e.getMessage());
		}
		return cmpMsg;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.viachat.dsservice.api.IDatastoreAPI#getComponentMessageByIncExcList
	 * (com.communication.message.IncExcListDTO)
	 */
	public List<ComponentMessage> getComponentMessageByIncExcList(
			IncExcListDTO incExcListDTO) throws DSException, Exception {
		logger.trace("Inside DatastorePersistence :: getComponentMessageByIncExcList()");
		if (incExcListDTO.getSid() == null && incExcListDTO.getSid() == "") {
			logger.info("While fetching  entity by recipient sid and include list :: Recipient sid cannot be empty ");
			throw new Exception("Recipient sid cannot be empty");
		} else if (incExcListDTO.getTimestamp() == null
				|| incExcListDTO.getTimestamp2() == null) {
			logger.info("While fetching  entity by recipient sid and include list :: Timestamp cannot be null ");
			throw new Exception("Timestamp cannot be null");
		} else if (incExcListDTO.getIncList() == null
				&& incExcListDTO.getIncList().isEmpty()) {
			logger.info("While fetching  entity by recipient sid and include list :: include list cannot be null and empty");
			throw new Exception("include list cannot be null and empty");
		}
		List<ComponentMessage> cmpMsg = null;
		try {
			cmpMsg = ObjectifyService.ofy().load().type(ComponentMessage.class)
			.filter("recipientDetails.recipientSid =",
			 incExcListDTO.getSid())
			// .filter("expiresOn =", null)
			.filter("timestamp =", incExcListDTO.getTimestamp())
			// .filter("timestamp <", incExcListDTO.getTimestamp2())
			// .filter("messageContent !=", incExcListDTO.getExcList())
					.filter("messageContent.type in",
							incExcListDTO.getIncList()).list();

		} catch (Exception e) {
			logger.error("Error while fetching  entity by include list",
					e.getMessage());
			throw new Exception(
					"Error while fetching  entity by include list {} "
							+ e.getMessage());
		}
		return cmpMsg;
	}
}
