
package com.viachat.dsservice.entity;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Unindex;

// TODO: Auto-generated Javadoc
/**
 * The Class Author.
 */	
@Unindex
public class Author implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The string id **/
	String sid;

	/** The first name. */
	String firstName;
	
	/** The last name. */
	String lastName;
	
	/** The photo url. */
	String photoUrl;
	
	/** The href. */
	private String href;

	/** The requestor location. */
	private String requestorLocation;
	
	/** The request location. */
	private String requestLocation;
	
	/** The customer email id. */
	private String emailId;
	
	/** The customer phone no. **/
	private String phoneNo;

	/**
	 * The Enum AuthorType.
	 */
	public enum AuthorType{
		
		/** The Agent. */
		Agent,
		
		/** The Client. */
		Client,
		
		/** The Supervisor. */
		Supervisor
	}

	/** The author type. */
	private AuthorType authorType;
	/**
	 * Gets the first name.
	 *
	 * @return the first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the first name.
	 *
	 * @param firstName the new first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the last name.
	 *
	 * @param lastName the new last name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the photo url.
	 *
	 * @return the photo url
	 */
	public String getPhotoUrl() {
		return photoUrl;
	}

	/**
	 * Sets the photo url.
	 *
	 * @param photoUrl the new photo url
	 */
	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	/**
	 * Instantiates a new author.
	 */
	public Author() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Instantiates a new author.
	 *
	 * @param firstName the first name
	 * @param lastName the last name
	 * @param photoUrl the photo url
	 */
	public Author(String firstName, String lastName, String photoUrl) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.photoUrl = photoUrl;
	}

	/**
	 * Gets the href.
	 *
	 * @return the href
	 */
	public String getHref() {
		return href;
	}

	/**
	 * Sets the href.
	 *
	 * @param href the new href
	 */
	public void setHref(String href) {
		this.href = href;
	}

	/**
	 * Gets the requestor location.
	 *
	 * @return the requestor location
	 */
	public String getRequestorLocation() {
		return requestorLocation;
	}

	/**
	 * Sets the requestor location.
	 *
	 * @param requestorLocation the new requestor location
	 */
	public void setRequestorLocation(String requestorLocation) {
		this.requestorLocation = requestorLocation;
	}

	/**
	 * Gets the request location.
	 *
	 * @return the request location
	 */
	public String getRequestLocation() {
		return requestLocation;
	}

	/**
	 * Sets the request location.
	 *
	 * @param requestLocation the new request location
	 */
	public void setRequestLocation(String requestLocation) {
		this.requestLocation = requestLocation;
	}

	/**
	 * Gets the author type.
	 *
	 * @return the author type
	 */
	public AuthorType getAuthorType() {
		return authorType;
	}

	/**
	 * Sets the author type.
	 *
	 * @param authorType the new author type
	 */
	public void setAuthorType(AuthorType authorType) {
		this.authorType = authorType;
	}

	
	/**
	 * Gets the email id.
	 *
	 * @return the email id
	 */
	public String getEmailId() {
		return emailId;
	}

	
	/**
	 * Sets the email id.
	 *
	 * @param emailId the new email id
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

}
