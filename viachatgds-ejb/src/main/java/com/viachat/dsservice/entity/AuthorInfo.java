package com.viachat.dsservice.entity;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.condition.IfNotNull;

@Index(IfNotNull.class)
public class AuthorInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1884722340823200258L;

	public enum AuthorType{
		C, // Represents a customer in a Business chat
		P,// Represents the users in a Personal Chat
		B,// Represents the Business (projectSid) in a Business Chat
		G ,//Represent the GroupSid in a Group Chat
		S//Represent the author to be a system
	}
	
	@Index(IfNotNull.class)
	private String sid;
	
	@Index(IfNotNull.class)
	private AuthorType type;

	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	public AuthorType getType() {
		return type;
	}
	public void setType(AuthorType type) {
		this.type = type;
	}
	
	public AuthorInfo() {
	}
	
	public AuthorInfo(String sid,AuthorType type) {
		this.sid=sid;
		this.type=type;
	}
}
