package com.viachat.dsservice.entity;

import java.io.Serializable;
import java.util.Date;


// TODO: Auto-generated Javadoc
/**
 * The Class LastMessageJsonInfo.
 */
public class LastMessageJsonInfo implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6712284749218520083L;
	
	/** The last message text. */
	private String lastMessageText;
	
	/** The timestamp. */
	private Date timestamp;
	
	/** The last message sender sid. */
	private String lastMessageSenderSid;
	
	/** The last message sender name. */
	private String lastMessageSenderName;
	
	/** The custom json attributes. */
	private CustomJsonAttributes customJsonAttributes;
	
	/**
	 * Gets the last message text.
	 *
	 * @return the last message text
	 */
	public String getLastMessageText() {
		return lastMessageText;
	}
	
	/**
	 * Sets the last message text.
	 *
	 * @param lastMessageText the new last message text
	 */
	public void setLastMessageText(String lastMessageText) {
		this.lastMessageText = lastMessageText;
	}
	
	/**
	 * Gets the timestamp.
	 *
	 * @return the timestamp
	 */
	public Date getTimestamp() {
		return timestamp;
	}
	
	/**
	 * Sets the timestamp.
	 *
	 * @param timestamp the new timestamp
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	
	/**
	 * Gets the last message sender sid.
	 *
	 * @return the last message sender sid
	 */
	public String getLastMessageSenderSid() {
		return lastMessageSenderSid;
	}
	
	/**
	 * Sets the last message sender sid.
	 *
	 * @param lastMessageSenderSid the new last message sender sid
	 */
	public void setLastMessageSenderSid(String lastMessageSenderSid) {
		this.lastMessageSenderSid = lastMessageSenderSid;
	}
	
	/**
	 * Gets the last message sender name.
	 *
	 * @return the last message sender name
	 */
	public String getLastMessageSenderName() {
		return lastMessageSenderName;
	}
	
	/**
	 * Sets the last message sender name.
	 *
	 * @param lastMessageSenderName the new last message sender name
	 */
	public void setLastMessageSenderName(String lastMessageSenderName) {
		this.lastMessageSenderName = lastMessageSenderName;
	}
	
	/**
	 * Instantiates a new last message json info.
	 */
	public LastMessageJsonInfo() {
	}
	
	/**
	 * Gets the custom json attributes.
	 *
	 * @return the custom json attributes
	 */
	public CustomJsonAttributes getCustomJsonAttributes() {
		return customJsonAttributes;
	}
	
	/**
	 * Sets the custom json attributes.
	 *
	 * @param customJsonAttributes the new custom json attributes
	 */
	public void setCustomJsonAttributes(CustomJsonAttributes customJsonAttributes) {
		this.customJsonAttributes = customJsonAttributes;
	}

	@Override
	public String toString() {
		return "LastMessageJsonInfo [lastMessageText=" + lastMessageText
				+ ", timestamp=" + timestamp + ", lastMessageSenderSid="
				+ lastMessageSenderSid + ", lastMessageSenderName="
				+ lastMessageSenderName + "]";
	}
}