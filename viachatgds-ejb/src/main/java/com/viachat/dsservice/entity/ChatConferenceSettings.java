package com.viachat.dsservice.entity;

import java.io.Serializable;

public class ChatConferenceSettings implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2658511195583180016L;

	private String confName;
	private String confId;

	public String getConfName() {
		return confName;
	}

	public void setConfName(String confName) {
		this.confName = confName;
	}

	public String getConfId() {
		return confId;
	}

	public void setConfId(String confId) {
		this.confId = confId;
	}
	
	public ChatConferenceSettings(){
		
	}

	public ChatConferenceSettings(String confName,String confId){
		this.confId = confId;
		this.confName = confName;
	}

}
