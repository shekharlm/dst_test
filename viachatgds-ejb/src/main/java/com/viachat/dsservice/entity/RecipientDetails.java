package com.viachat.dsservice.entity;

import java.io.Serializable;

import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.condition.IfNotNull;

@Index(IfNotNull.class)
public class RecipientDetails implements Serializable {

	private static final long serialVersionUID = 4335701566616010015L;

	public enum RecipientType {
		U,  /** to specify USER (customer/agents) */
		G,  /** to specify GROUPS   */
		P,  /** to specify PROJECT */
		AG	/** to specify AD-HOC GROUPS */
	}

	@Index(IfNotNull.class)
	private String recipientSid;
	
	@Index(IfNotNull.class)
	private RecipientType recipientType;

	public String getRecipientSid() {
		return recipientSid;
	}

	public void setRecipientSid(String recipientSid) {
		this.recipientSid = recipientSid;
	}

	public RecipientType getRecipientType() {
		return recipientType;
	}

	public void setRecipientType(RecipientType recipientType) {
		this.recipientType = recipientType;
	}
	
	public RecipientDetails(String recipientSid,RecipientType recipientType) {
		this.recipientSid=recipientSid;
		this.recipientType=recipientType;
	}

	public RecipientDetails() {
		this.recipientSid="";
		this.recipientType=RecipientType.U;
	}
}
