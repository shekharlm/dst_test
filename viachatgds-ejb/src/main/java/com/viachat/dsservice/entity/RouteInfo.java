package com.viachat.dsservice.entity;

import java.io.Serializable;

public class RouteInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7290460327934121169L;

	public enum RouteType {
		DEFAULT, QUEUE, GROUP, AGENT,ALLOW_CONF
	}

	/** The routeType. */
	private RouteType routeType;

	/** The routeParam. */
	private String routeParam;

	private Integer routeActionId;

	/**
	 * Gets the routeType
	 * 
	 * @return the routeType
	 */
	public RouteType getRouteType() {
		return routeType;
	}

	/**
	 * Sets the routeType
	 * 
	 * @param the routeType
	 */
	public void setRouteType(RouteType routeType) {
		this.routeType = routeType;
	}

	/**
	 * Gets the routeParam
	 * 
	 * @return the routeParam
	 */
	public String getRouteParam() {
		return routeParam;
	}

	/**
	 * Sets the routeParam
	 * 
	 * @param the routeParam
	 */
	public void setRouteParam(String routeParam) {
		this.routeParam = routeParam;
	}

	public Integer getRouteActionId() {
		return routeActionId;
	}

	public void setRouteActionId(Integer routeActionId) {
		this.routeActionId = routeActionId;
	}
}
