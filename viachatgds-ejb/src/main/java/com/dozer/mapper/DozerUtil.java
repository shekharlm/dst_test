package com.dozer.mapper;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Startup;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Singleton;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

@Singleton
@Startup
public class DozerUtil {
	private final DozerBeanMapper dozerBeanMapper = new DozerBeanMapper();
	
	@Produces @DozerMapper
	public Mapper getDozerBeanMapper(){
		return dozerBeanMapper;
	}
	
	@PostConstruct
	public void init(){
		List<String> mappingList = new ArrayList<String>();
		mappingList.add("dozerBeanMapping.xml");
	}
	
	@Inject
	@DozerMapper
	Mapper mapper;
	public <T> T convert(Object srcObject, Class<T> destnationObj) throws Exception{
		try{
		     return (T) mapper.map(srcObject, destnationObj);
		}catch(Exception exception){
			exception.printStackTrace();
			throw new Exception();
		}
		//return  convert(srcObject,destnationObj,null);
	}
	public <T> T convert(Object srcObject, Class<T> destnationObj,String mapId) throws Exception{
		try{
			return (T) mapper.map(srcObject, destnationObj, mapId);
		}catch(Exception exception){
			throw new Exception();
		}
	}
	
	public <T> T convert(Object srcObject, T destObject)throws Exception{
		try {
			mapper.map(srcObject, destObject);
		} catch (Exception exception) {
			throw new Exception();
		}
		return destObject;
	}
	
	/**
	 * A generic method to convert list of srcObject to list of destObject
	 * 
	 * @param srcObjects
	 *            - List of object to be converted.
	 * @param destnationObjClass
	 *            -class of destinationObject for conversion.
	 * @return List of destnationObj
	 * @throws ApplicationException
	 */
	public <T, U> List<T> convertList(List<U> srcObjects, Class<T> destnationObjClass) throws Exception {
		return convertList(srcObjects,destnationObjClass,null);
	}

	/**
	 * A generic method to convert list of srcObject to list of destObject by
	 * using dozer-mapper ID.
	 * 
	 * @param srcObjects
	 *            - List of object to be converted.
	 * @param destnationObjClass
	 *            -class of destinationObject for conversion.
	 * @param mapId
	 *            - Dozer map Id for conversion.
	 * @return List of destnationObj
	 * @throws ApplicationException
	 */
	public <T, U> List<T> convertList(List<U> srcObjects, Class<T> destnationObjClass,String mapId)
			throws Exception {
		List<T> list = null;
		try {
			if (srcObjects != null) {
				list = new ArrayList<T>();
				if (mapId == null) {
					for (U srcObject : srcObjects) {
						list.add(convert(srcObject, destnationObjClass));
					}
				} else {
					for (U srcObject : srcObjects) {
						list.add(convert(srcObject, destnationObjClass, mapId));
					}
				}
			} else {
				throw new Exception();//List of object to be converted is null.
			}
		} catch (Exception applicationException) {
			throw applicationException;
		}
		return list;
	}
}

