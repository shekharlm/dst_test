package com.notification.message.content;

import java.io.Serializable;


/**
 * The Class Attachment.
 */
public class AttachmentDTO implements Serializable{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -5132567868232991513L;
	
	/** The sid. */
	private String sid;
	
	/** The href. */
	private String href;
	
	private String attachmentDetailJson;
	
	private String thumbnail;
	/**
	 * Gets the href.
	 *
	 * @return the href
	 */
	public String getHref() {
		return href;
	}
	
	/**
	 * Sets the href.
	 *
	 * @param href the new href
	 */
	public void setHref(String href) {
		this.href = href;
	}
	
	/**
	 * Gets the sid.
	 *
	 * @return the sid
	 */
	public String getSid() {
		return sid;
	}
	/**
	 * Sets the sid.
	 *
	 * @param sid the new sid
	 */
	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getAttachmentDetailJson() {
		return attachmentDetailJson;
	}

	public void setAttachmentDetailJson(String attachmentDetailJson) {
		this.attachmentDetailJson = attachmentDetailJson;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
}
