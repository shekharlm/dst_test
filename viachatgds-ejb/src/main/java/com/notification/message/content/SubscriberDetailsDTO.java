package com.notification.message.content;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class SubscriberDetails.
 */
public class SubscriberDetailsDTO<T> implements Serializable {
	
	public static final String ACTION_SUBSCRIBE="subscribe",ACTION_UNSUBSCRIBE="unsubscribe";
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The topic name. */
	private String action, topicName,type;
	
	private String userType;

	/**
	 * Gets the action.
	 *
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * Sets the action.
	 *
	 * @param action the new action
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * Gets the topic name.
	 *
	 * @return the topic name
	 */
	public String getTopicName() {
		return topicName;
	}

	/**
	 * Sets the topic name.
	 *
	 * @param topicName the new topic name
	 */
	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

}
