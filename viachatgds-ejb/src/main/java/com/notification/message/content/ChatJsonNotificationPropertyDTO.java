package com.notification.message.content;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.communication.destination.ProjectKey;
import com.communication.message.content.ChatActionMessageDTO.ChatDirection;



// TODO: Auto-generated Javadoc
/**
 * The Class ChatJsonNotificationProperty.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChatJsonNotificationPropertyDTO  implements Serializable {
	
	/** The Constant AGENT_REQUEST. */
	public static final String AGENT_REQUEST = "agent-request";
	
	/** The Constant CLIENT_PROGRESS. */
	public static final String CLIENT_PROGRESS = "client-chatProgress";
	
	/** The Constant AGENT_DECLINED. */
	public static final String AGENT_DECLINED = "agent-decline";
	
	/** The Constant AGENT_ACCEPTED. */
	public static final String AGENT_ACCEPTED = "agent-accept";
	
	/** The Constant CHAT_ABANDONED. */
	public static final String CHAT_ABANDONED = "chat-abandoned";
	
	/** The Constant CHAT_ENDED. */
	public static final String CHAT_ENDED = "chat-ended";
	
	/** The Constant CHAT_RESPONDED. */
	public static final String CHAT_RESPONDED = "chat-responded";
	
	/** The Constant CHAT_POST. */
	public static final String CHAT_POST = "chat-post";
	
	/** The Constant CHAT_ACTION. */
	public static final String CHAT_ACTION="chat-action";
	
	/** The Constant CHAT_SUBUNSUB. */
	public static final String CHAT_SUBUNSUB="subunsub";
	
	/** The Constant CHAT_STATUS. */
	public static final String CHAT_STATUS = "chat-status";
	
	/** The Constant CHAT_REMOVED. */
	public static final String CHAT_REMOVED = "chat-remove";
	
	/** The Constant CHAT_EXIT. */
	public static final String CHAT_EXIT = "chat-exit";
	
	/** The Constant CHAT_TRANSFER_REQUEST. */
	public static final String CHAT_TRANSFER_REQUEST="chat-transfer-request";
	
	/** The Constant CHAT_TRANSFER_ACCEPT. */
	public static final String CHAT_TRANSFER_ACCEPT="chat-transfer-accept";
	
	/** The Constant CHAT_TRANSFER_DECLINE. */
	public static final String CHAT_TRANSFER_DECLINE="chat-transfer-decline";
	
	/** The Constant CHAT_CONFERENCE_REQUEST. */
	public static final String CHAT_CONFERENCE_REQUEST="chat-conference-request";
	
	/** The Constant CHAT_CONFERENCE_ACCEPT. */
	public static final String CHAT_CONFERENCE_ACCEPT="chat-conference-accept";
	
	/** The Constant CHAT_CONFERENCE_DECLINE. */
	public static final String CHAT_CONFERENCE_DECLINE="chat-conference-decline";
	
	/** The Constant CHAT_ATTACH. */
	public static final String CHAT_ATTACH="chat-attach";
	
	/** The Constant CHAT_PROMPT. */
	public static final String CHAT_PROMPT="chat-prompt";
	
	/** The Constant CHAT_PROMPT_WAIT. */
	public static final String CHAT_PROMPT_WAIT="chat-prompt-wait";
	
	/** The Constant CHAT_PROMPT_END. */
	public static final String CHAT_PROMPT_END="chat-prompt-end";
	
	/** The Constant CHAT_LEAVE_MESSAGE. */
	public static final String CHAT_LEAVE_MESSAGE="chat-leave-message";
	
	/** The Constant CHAT_LEAVE_MESSAGE. */
	public static final String CHAT_LEAVE_MESSAGE_OPT="chat-leave-message-opt";

	/** The Constant CHAT_LEAVE_MESSAGE_ACCEPT. */
	public static final String CHAT_LEAVE_MESSAGE_ACCEPT="chat-leave-message-accept";
	
	/** The Constant CHAT_LEAVE_MESSAGE_REJECT. */
	public static final String CHAT_LEAVE_MESSAGE_REJECT="chat-leave-message-reject";
	
	/** The Constant TAP_AGENT_REQUEST. */
	public static final String TAP_AGENT_REQUEST = "tap-agent-request";

	/** The Constant AGENT_FOUND. */
	public static final String AGENT_FOUND = "agent-found";
	
	/** The Constant CALLBACK_AGENT_REQUEST. */
	public static final String CALLBACK_AGENT_REQUEST = "callback-agent-request";

	/** The Constant TAP_ONLY_AGENT_REQUEST. */
	public static final String TAP_ONLY_AGENT_REQUEST = "tap-only-agent-request";

	/** The Constant CALLBACK_ONLY_AGENT_REQUEST. */
	public static final String CALLBACK_ONLY_AGENT_REQUEST = "callback-only-agent-request";

	/** The Constant AGENT_FOUND_CALL_ONLY. */
	public static final String AGENT_FOUND_CALL_ONLY = AGENT_FOUND.concat("-call-only");
	
	/** The Constant AGENT_FOUND_CHAT_ONLY. */
	public static final String AGENT_FOUND_CHAT_ONLY = AGENT_FOUND.concat("-chat-only");

	/** The Constant PUSH_COMMAND. */
	public static final String PUSH_COMMAND = "push-command";
	
	/** The Constant AGENT_POKE. */
	public static final String AGENT_POKE = "agent-poke";
	
	/** The Constant POST. */
	public static final String POST_REQ="post-req";
	
	/** The Constant MESSAGE_STATUS. */
	public static final String MESSAGE_STATUS="message-status";
	
	/** The Constant SYSTEM_MESSAGE. This is used to identify a message as  a system message*/
	public static final String SYSTEM_MESSAGE="sys-mesg";
	
	
	/** The Constant PUSH_FORM. */
	public static final String PUSH_FORM="push-form";
	
	/**
	 * The Enum ChatSubType.
	 */
	public enum ChatSubType{
		
		/** The requested. */
		REQUESTED,
		
		/** The submitted. */
		SUBMITTED,
		
		/** The tracking. */
		TRACKING,
		
		/** The hiddensubmit. */
		HIDDENSUBMIT
	}

	
	/**
	 * The Enum ChatOriginationType.
	 */
	public enum ChatOriginationType{
		
		/** The internal. */
		INTERNAL,
		
		/** The external. */
		EXTERNAL
	}
	
	/**
	 * The Enum ChatSubStatus.
	 */
	public enum ChatSubStatus{
		
		/** The bargein. */
		BARGEIN,
		
		/** The listen. */
		LISTEN,
		
		/** The conference. */
		CONFERENCE,

		/** The transfer. */
		TRANSFER,
		
		/** The call waiter. */
		CALL_WAITER,
		
		/** The delivered. */
		DELIVERED,
		
		/** The read. */
		READ,
		
		/** The order. */
		ORDER
		
	}
	
	/**
	 * The Enum RecieverType.
	 */
	public enum RecieverType{
		
		/** The Agent. */
		Agent,
		
		/** The Client. */
		Client,
		
		/** The Supervisor. */
		Supervisor
	}
	
	/**
	 * The Enum DeviceType.
	 */
	public enum DeviceType{
		
		/** The mobile browser. */
		MOBILE_BROWSER,
		
		/** The desktop browser. */
		DESKTOP_BROWSER,
		
		/** The chrome extensions. */
		CHROME_EXTENSIONS
	}
	
	/**
	 * The Enum DeviceType.
	 */
	public enum AgentType {
		
		/** The auto agent user. */
		AUTO_AGENT_USER, 
	 /** The general user. */
	 GENERAL_USER
	}


	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 35982078011618111L;
	
	/** The message txt. */
	private String chatSessionId, type, messageTxt;
	
	/** The chat message id. */
	private String chatMessageId;
	
	/** The mesg id for ch8 stats. */
	private String mesgIdForCh8Stats;
	
	/** The author. */
	private AuthorDTO author;
	
	/** The attachment. */
	private AttachmentDTO attachment;
	
	/** The origination type. */
	private ChatOriginationType originationType;
	
	/** The reciever type. */
	private RecieverType recieverType;
	
	/** The prompt. */
	private String prompt;
	
	/** The route info. */
	private RouteInfoDTO routeInfo;

	/** The sub status. */
	private ChatSubStatus subStatus;
	
	/** The mobile support. */
	private boolean mobileSupport;
	
	/** The device type. */
	private DeviceType deviceType;
	
	/** The chat sub type. */
	private ChatSubType chatSubType;
	
	/** The agent type. */
	private AgentType agentType;

	/** The last message json info. */
	private LastMessageJsonInfoDTO lastMessageJsonInfo;
	
	/** The message level. */
	private Integer messageLevel;
	
	/** The conf config.  This will hold the information for the Conference Configuration*/
	private ChatConferenceSettingsDTO confConfig;

	/** The chat direction. */
	private ChatDirection chatDirection;
	
	/** The web form sid. */
	private String webFormSid;
	
	/** The p key. */
	private ProjectKey pKey; 
	
	
	/**
	 * Instantiates a new chat json notification property.
	 */
	public ChatJsonNotificationPropertyDTO() {
	}
	
	/**
	 * Instantiates a new chat json notification property.
	 *
	 * @param chatSessionId the chat session id
	 * @param type the type
	 * @param messageTxt the message txt
	 * @param author the author
	 */
	public ChatJsonNotificationPropertyDTO(String chatSessionId, String type,
			String messageTxt, AuthorDTO author) {
		super();
		this.chatSessionId = chatSessionId;
		this.type = type;
		this.messageTxt = messageTxt;
		this.author = author;
	}

	/**
	 * Gets the chat session id.
	 *
	 * @return the chat session id
	 */
	public String getChatSessionId() {
		return chatSessionId;
	}

	/**
	 * Sets the chat session id.
	 *
	 * @param chatSessionId the new chat session id
	 */
	public void setChatSessionId(String chatSessionId) {
		this.chatSessionId = chatSessionId;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the message txt.
	 *
	 * @return the message txt
	 */
	public String getMessageTxt() {
		return messageTxt;
	}

	/**
	 * Sets the message txt.
	 *
	 * @param messageTxt the new message txt
	 */
	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	/**
	 * Gets the author.
	 *
	 * @return the author
	 */
	public AuthorDTO getAuthor() {
		return author;
	}

	/**
	 * Sets the author.
	 *
	 * @param author the new author
	 */
	public void setAuthor(AuthorDTO author) {
		this.author = author;
	}

	/**
	 * Gets the reciever type.
	 *
	 * @return the reciever type
	 */
	public RecieverType getRecieverType() {
		return recieverType;
	}

	/**
	 * Sets the reciever type.
	 *
	 * @param recieverType the new reciever type
	 */
	public void setRecieverType(RecieverType recieverType) {
		this.recieverType = recieverType;
	}

	/**
	 * Gets the origination type.
	 *
	 * @return the origination type
	 */
	public ChatOriginationType getOriginationType() {
		return originationType;
	}

	/**
	 * Sets the origination type.
	 *
	 * @param originationType the new origination type
	 */
	public void setOriginationType(ChatOriginationType originationType) {
		this.originationType = originationType;
	}

	/**
	 * Gets the attachment.
	 *
	 * @return the attachment
	 */
	public AttachmentDTO getAttachment() {
		return attachment;
	}

	/**
	 * Sets the attachment.
	 *
	 * @param attachment the new attachment
	 */
	public void setAttachment(AttachmentDTO attachment) {
		this.attachment = attachment;
	}

	/**
	 * Gets the prompt.
	 *
	 * @return the prompt
	 */
	public String getPrompt() {
		return prompt;
	}

	/**
	 * Sets the prompt.
	 *
	 * @param prompt the new prompt
	 */
	public void setPrompt(String prompt) {
		this.prompt = prompt;
	}

	/**
	 * Gets the routeInfo.
	 * 
	 * @return the routeInfo
	 */
	public RouteInfoDTO getRouteInfo() {
		return routeInfo;
	}

	/**
	 * Sets the routeInfo.
	 *
	 * @param routeInfo the new route info
	 * @return the routeInfo
	 */
	public void setRouteInfo(RouteInfoDTO routeInfo) {
		this.routeInfo = routeInfo;
	}

	/**
	 * Gets the sub status.
	 *
	 * @return the sub status
	 */
	public ChatSubStatus getSubStatus() {
		return subStatus;
	}

	/**
	 * Sets the sub status.
	 *
	 * @param subStatus the new sub status
	 */
	public void setSubStatus(ChatSubStatus subStatus) {
		this.subStatus = subStatus;
	}

	/**
	 * Checks if is mobile support.
	 *
	 * @return true, if is mobile support
	 */
	public boolean getMobileSupport() {
		return mobileSupport;
	}

	/**
	 * Sets the mobile support.
	 *
	 * @param mobileSupport the new mobile support
	 */
	public void setMobileSupport(boolean mobileSupport) {
		this.mobileSupport = mobileSupport;
	}

	/**
	 * Gets the device type.
	 *
	 * @return the device type
	 */
	public DeviceType getDeviceType() {
		return deviceType;
	}

	/**
	 * Sets the device type.
	 *
	 * @param deviceType the new device type
	 */
	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}

	/**
	 * Gets the chat sub type.
	 *
	 * @return the chat sub type
	 */
	public ChatSubType getChatSubType() {
		return chatSubType;
	}

	/**
	 * Sets the chat sub type.
	 *
	 * @param chatSubType the new chat sub type
	 */
	public void setChatSubType(ChatSubType chatSubType) {
		this.chatSubType = chatSubType;
	}
	
	/**
	 * Gets the last message json info.
	 *
	 * @return the last message json info
	 */
	public LastMessageJsonInfoDTO getLastMessageJsonInfo() {
		return lastMessageJsonInfo;
	}

	/**
	 * Sets the last message json info.
	 *
	 * @param lastMessageJsonInfo the new last message json info
	 */
	public void setLastMessageJsonInfo(LastMessageJsonInfoDTO lastMessageJsonInfo) {
		this.lastMessageJsonInfo = lastMessageJsonInfo;
	}

	/**
	 * Gets the agent type.
	 *
	 * @return the agent type
	 */
	public AgentType getAgentType() {
		return agentType;
	}

	/**
	 * Sets the agent type.
	 *
	 * @param agentType the new agent type
	 */
	public void setAgentType(AgentType agentType) {
		this.agentType = agentType;
	}

	/**
	 * Gets the message level.
	 *
	 * @return the message level
	 */
	public Integer getMessageLevel() {
		return messageLevel;
	}

	/**
	 * Sets the message level.
	 *
	 * @param messageLevel the new message level
	 */
	public void setMessageLevel(Integer messageLevel) {
		this.messageLevel = messageLevel;
	}

	/**
	 * Gets the conf config.
	 *
	 * @return the conf config
	 */
	public ChatConferenceSettingsDTO getConfConfig() {
		return confConfig;
	}

	/**
	 * Sets the conf config.
	 *
	 * @param confConfig the new conf config
	 */
	public void setConfConfig(ChatConferenceSettingsDTO confConfig) {
		this.confConfig = confConfig;
	}

	 
	/**
	 * Gets the chat direction.
	 *
	 * @return the chat direction
	 */
	public ChatDirection getChatDirection() {
		return chatDirection;
	}

	/**
	 * Sets the chat direction.
	 *
	 * @param chatDirection the new chat direction
	 */
	public void setChatDirection(ChatDirection chatDirection) {
		this.chatDirection = chatDirection;
	}

	/**
	 * Gets the mesg id for ch8 stats.
	 *
	 * @return the mesg id for ch8 stats
	 */
	public String getMesgIdForCh8Stats() {
		return mesgIdForCh8Stats;
	}

	/**
	 * Sets the mesg id for ch8 stats.
	 *
	 * @param mesgIdForCh8Stats the new mesg id for ch8 stats
	 */
	public void setMesgIdForCh8Stats(String mesgIdForCh8Stats) {
		this.mesgIdForCh8Stats = mesgIdForCh8Stats;
	}

	/**
	 * Gets the chat message id.
	 *
	 * @return the chat message id
	 */
	public String getChatMessageId() {
		return chatMessageId;
	}

	/**
	 * Sets the chat message id.
	 *
	 * @param chatMessageId the new chat message id
	 */
	public void setChatMessageId(String chatMessageId) {
		this.chatMessageId = chatMessageId;
	}

	/**
	 * Gets the web form sid.
	 *
	 * @return the web form sid
	 */
	public String getWebFormSid() {
		return webFormSid;
	}

	/**
	 * Sets the web form sid.
	 *
	 * @param webFormSid the new web form sid
	 */
	public void setWebFormSid(String webFormSid) {
		this.webFormSid = webFormSid;
	}

	/**
	 * Gets the p key.
	 *
	 * @return the p key
	 */
	public ProjectKey getpKey() {
		return pKey;
	}

	/**
	 * Sets the p key.
	 *
	 * @param pKey the new p key
	 */
	public void setpKey(ProjectKey pKey) {
		this.pKey = pKey;
	}
	
}

