package com.notification.message.content;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * The Class CustomJsonAttributes.
 * 
 * This class will be useful to pass a Map of field,value attributes 
 * and the benefit of the class will be that the common modules of componentframework, jms producer etc will not be required to be deployed at every time a new attribute is added
 * Only the implementation module which uses this code can be changed and implemented
 *
 */
public class CustomJsonAttributesDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2550153816287495951L;
	
	/** The json attr map. */
	private Map<String, String> jsonAttrMap;
	
	/**
	 * Gets the json attrs.
	 *
	 * @return the json attrs
	 */
	public Map<String, String> getJsonAttrs() {
		return jsonAttrMap;
	}
	
	/**
	 * Sets the json attrs.
	 *
	 * @param jsonAttrs the json attrs
	 */
	public void setJsonAttrs(Map<String, String> jsonAttrs) {
		this.jsonAttrMap = jsonAttrs;
	}
	
	/**
	 * Instantiates a new custom json attributes.
	 */
	public CustomJsonAttributesDTO() {
		jsonAttrMap = new HashMap<String, String>();
	}
	
	/**
	 * With attribute.
	 *
	 * @param fieldName the field name
	 * @param value the value
	 */
	public void withAttribute(String fieldName, String value){
		if(jsonAttrMap!=null && !jsonAttrMap.containsKey(fieldName)){
			jsonAttrMap.put(fieldName, value);
		}
	}
	
	/**
	 * Gets the attribute map.
	 *
	 * @return the attribute map
	 */
	public Map<String,String> getAttributeMap(){
		return jsonAttrMap;
	}
}
