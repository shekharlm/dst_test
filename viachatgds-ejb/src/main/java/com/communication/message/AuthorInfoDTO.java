package com.communication.message;

import java.io.Serializable;

public class AuthorInfoDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1884722340823200258L;

	public enum AuthorType{
		C, // Represents a customer in a Business chat
		P,// Represents the users in a Personal Chat
		B,// Represents the Business (projectSid) in a Business Chat
		G ,//Represent the GroupSid in a Group Chat
		S//Represent the author to be a system
	}
	
	private String sid;
	private AuthorType type;

	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	public AuthorType getType() {
		return type;
	}
	public void setType(AuthorType type) {
		this.type = type;
	}
	
	public AuthorInfoDTO() {
	}
	
	public AuthorInfoDTO(String sid,AuthorType type) {
		this.sid=sid;
		this.type=type;
	}
}
