package com.communication.message;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.notification.message.content.ChatJsonNotificationPropertyDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class ComponentMessage class provides a skeleton to send messages between
 * various components with the actual content being provided as required.
 *
 * @param <T>
 *            the generic type
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ComponentMessageDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1145126112169812577L;

	/**
	 * The Enum MessageType.
	 */
	public enum MessageType {

		/** The Agent activity. */
		AgentActivity,

		/** The Agent status. */
		AgentStatus,

		/** The Call activity. */
		CallActivity,

		/** The Call activity. */
		CallInfoStatus,

		/** The Call queued. */
		CallQueued,

		/** The Call status. */
		CallStatus,

		/** The Case activity. */
		CaseActivity,

		/** The Case status. */
		CaseStatus,

		/** The Chat activity. */
		ChatActivity,

		/** The Chat queued. */
		ChatQueued,

		/** The Chat request. */
		ChatRequest,

		/** The Chat request response. */
		ChatRequestResponse,

		/** The Chat status. */
		ChatStatus,

		/** The Chat transfer. */
		ChatTransfer,

		/** The Chat action. */
		ChatAction,

		/** The Chat message. */
		ChatMessage,

		/** The Subscribe notification. */
		SubscribeNotification,

		/** Timer service released messages. */
		TimerRequest,

		/** The Timer cancel request. */
		TimerCancelRequest,

		/** The Timer notification. */
		TimerNotification,

		/** The Call record attachment. */
		CallRecordAttachment,

		/** The Offline case. */
		OfflineCase,

		/** The Presence updates. */
		PresenceUpdates,

		/** The Call log. */
		CallLog,

		/** The Case event. */
		CaseEvent,

		/** The Case touch. */
		CaseTouch,

		/** The Offline case creation status. */
		OfflineCaseCreationStatus,

		/** The Outbound case mail. */
		OutboundCaseMail,

		/** The Gui notification. */
		GuiNotification,

		/** Voice Message *. */
		VoiceMessage,

		/** The Chat leave message response. */
		ChatLeaveMessageResponse,

		/** The Chat leave message. */
		ChatLeaveMessage, // response from option to leave chat message

		/** Template message type *. */
		TemplateEventMessage,

		/** Email Topic *. */
		EmailTemplate,

		/** IN Template Topic *. */
		INTemplateMessage,

		/** The Routing queue. */
		RoutingQueue,

		/** The Call Text. */
		CallText,

		/** The Text message. */
		TextMessage,

		/** The Text message. */
		TextMessageError,

		/** The Send text message. */
		SendTextMessage,

		/** The Make call. */
		MakeCall,

		/** The Supervisor Logout *. */
		SupervisorAction,

		/** The Incoming email. */
		IncomingEmail,

		/** The Directory event. */
		DirectoryEvent,

		/** The Supervisor comment. */
		SupervisorComment,

		AgentActivityMessage,

		/** The Timed rule event. Notify event when a timed rule is fired */
		TimedRuleEvent,

		TelephonyCleanupEvent,

		/** The Send email. */
		SendEmail,

		UsageTrigger,

		EmailTracking,

		TaskNotifier,
		/**
		 * The Chat group. This message type represents group notification
		 * message
		 */
		GroupNotif,

		/** To send sms notification */
		SMSNotification, PaymentNotifier,

		/** To send pa component and ux notification */
		PA

	}

	/** The version. */
	private String version;

	/** The message id. */
	private String messageId;

	/** The type. */
	private MessageType type;

	/** The timestamp. */
	private Date timestamp;
	
	private Date timestamp2;

	/** The identification details. */
	private IdentificationDetailDTO identificationDetails;

	/** The message content. */
	private ChatJsonNotificationPropertyDTO messageContent;

	private RecipientDetailsDTO recipientDetails;

	private AuthorInfoDTO authorInfo;

	private int messagePriority;

	private Date expiresOn;

	/**
	 * Gets the message id.
	 *
	 * @return the message id
	 */
	public String getMessageId() {
		return messageId;
	}

	/**
	 * Sets the message id.
	 *
	 * @param messageId
	 *            the new message id
	 */
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public MessageType getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type
	 *            the new type
	 */
	public void setType(MessageType type) {
		this.type = type;
	}

	/**
	 * Gets the timestamp.
	 *
	 * @return the timestamp
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * Sets the timestamp.
	 *
	 * @param timestamp
	 *            the new timestamp
	 */
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * Gets the identification details.
	 *
	 * @return the identification details
	 */
	public IdentificationDetailDTO getIdentificationDetails() {
		return identificationDetails;
	}

	/**
	 * Sets the identification details.
	 *
	 * @param identificationDetails
	 *            the new identification details
	 */
	public void setIdentificationDetails(
			IdentificationDetailDTO identificationDetails) {
		this.identificationDetails = identificationDetails;
	}

	/**
	 * Gets the message content.
	 *
	 * @return the message content
	 */
	public ChatJsonNotificationPropertyDTO getMessageContent() {
		return messageContent;
	}

	/**
	 * Sets the message content.
	 *
	 * @param messageContent
	 *            the new message content
	 */
	public void setMessageContent(ChatJsonNotificationPropertyDTO messageContent) {
		this.messageContent = messageContent;
	}

	/**
	 * Gets the version.
	 *
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Sets the version.
	 *
	 * @param version
	 *            the new version
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	public int getMessagePriority() {
		return messagePriority;
	}

	public void setMessagePriority(int messagePriority) {
		this.messagePriority = messagePriority;
	}

	public RecipientDetailsDTO getRecipientDetails() {
		return recipientDetails;
	}

	public void setRecipientDetails(RecipientDetailsDTO recipientDetails) {
		this.recipientDetails = recipientDetails;
	}

	public AuthorInfoDTO getAuthorInfo() {
		return authorInfo;
	}

	public void setAuthorInfo(AuthorInfoDTO authorInfo) {
		this.authorInfo = authorInfo;
	}

	public Date getTimestamp2() {
		return timestamp2;
	}

	public void setTimestamp2(Timestamp timestamp2) {
		this.timestamp2 = timestamp2;
	}

	public Date getExpiresOn() {
		return expiresOn;
	}

	public void setExpiresOn(Date expiresOn) {
		this.expiresOn = expiresOn;
	}

	
}