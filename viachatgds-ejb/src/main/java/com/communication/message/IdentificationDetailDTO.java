package com.communication.message;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

// TODO: Auto-generated Javadoc
/**
 * The Class IdentificationDetails contains the information which is required to identify all the ID's for the messges.
 */
public class IdentificationDetailDTO implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The subscriber id. */
	private String subscriberId;
	
	/** The group id. */
	private String groupId;
	
	@NotNull
	/** The project id. */
	private String projectId;
	
	/** The company id. */
	private String companyId;
	
	/** The broadcaster id. */
	private String broadcasterId;
	
	/** The customer detail. */
	private CustomerDetailDTO customerDetails;

	/** The agent's email id - call-text **/
	private String agentEmailId;

    /**
     * The Tap session sid
     */
    private String sessionSid;

    private String agentName;

	private String agentPhotoUrl;
	
	private String caseSid;
	
	/**
	 * Gets the subscriber id.
	 *
	 * @return the subscriber id
	 */
	public String getSubscriberId() {
		return subscriberId;
	}
	
	/**
	 * Sets the subscriber id.
	 *
	 * @param subscriberId the new subscriber id
	 */
	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}
	
	/**
	 * Gets the group id.
	 *
	 * @return the group id
	 */
	public String getGroupId() {
		return groupId;
	}
	
	/**
	 * Sets the group id.
	 *
	 * @param groupId the new group id
	 */
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	
	/**
	 * Gets the project id.
	 *
	 * @return the project id
	 */
	public String getProjectId() {
		return projectId;
	}
	
	/**
	 * Sets the project id.
	 *
	 * @param projectId the new project id
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	
	/**
	 * Gets the company id.
	 *
	 * @return the company id
	 */
	public String getCompanyId() {
		return companyId;
	}
	
	/**
	 * Sets the company id.
	 *
	 * @param companyId the new company id
	 */
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	
	/**
	 * Gets the serialversionuid.
	 *
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	/**
	 * Resets the properties.
	 */
	public void reset(){
		subscriberId=null;
		groupId=null;
		projectId=null;
		companyId=null;
	}

	/**
	 * Gets the broadcaster id.
	 *
	 * @return the broadcaster id
	 */
	public String getBroadcasterId() {
		return broadcasterId;
	}

	/**
	 * Sets the broadcaster id.
	 *
	 * @param broadcasterId the new broadcaster id
	 */
	public void setBroadcasterId(String broadcasterId) {
		this.broadcasterId = broadcasterId;
	}

	/**
	 * Gets the customer detail.
	 *
	 * @return the customer detail
	 */
	public CustomerDetailDTO getCustomerDetails() {
		return customerDetails;
	}

	/**
	 * Sets the customer detail.
	 *
	 * @param customerDetail the new customer detail
	 */
	public void setCustomerDetails(CustomerDetailDTO customerDetail) {
		this.customerDetails = customerDetail;
	}

    public String getSessionSid() {
        return sessionSid;
    }

    public void setSessionSid(String sessionSid) {
        this.sessionSid = sessionSid;
    }

	public String getAgentEmailId() {
		return agentEmailId;
	}

	public void setAgentEmailId(String agentEmailId) {
		this.agentEmailId = agentEmailId;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAgentPhotoUrl() {
		return agentPhotoUrl;
	}

	public void setAgentPhotoUrl(String agentPhotoUrl) {
		this.agentPhotoUrl = agentPhotoUrl;
	}

	public String getCaseSid() {
		return caseSid;
	}

	public void setCaseSid(String caseSid) {
		this.caseSid = caseSid;
	}
	
}
