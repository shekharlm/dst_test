package com.communication.message;

import java.util.List;

public class IncExcListDTO extends TimestampDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8255029474976083142L;
	
	private List<String> incList;
	private List<String> excList;

	public List<String> getIncList() {
		return incList;
	}

	public void setIncList(List<String> incList) {
		this.incList = incList;
	}

	public List<String> getExcList() {
		return excList;
	}

	public void setExcList(List<String> excList) {
		this.excList = excList;
	}

}
