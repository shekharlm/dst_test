package com.communication.message;

import java.io.Serializable;

public class RecipientDetailsDTO implements Serializable {

	private static final long serialVersionUID = 4335701566616010015L;

	public enum RecipientType {
		U,  /** to specify USER (customer/agents) */
		G,  /** to specify GROUPS   */
		P,  /** to specify PROJECT */
		AG	/** to specify AD-HOC GROUPS */
	}

	private String recipientSid;
	private RecipientType recipientType;

	public String getRecipientSid() {
		return recipientSid;
	}

	public void setRecipientSid(String recipientSid) {
		this.recipientSid = recipientSid;
	}

	public RecipientType getRecipientType() {
		return recipientType;
	}

	public void setRecipientType(RecipientType recipientType) {
		this.recipientType = recipientType;
	}
	
	public RecipientDetailsDTO(String recipientSid,RecipientType recipientType) {
		this.recipientSid=recipientSid;
		this.recipientType=recipientType;
	}

	public RecipientDetailsDTO() {
		this.recipientSid="";
		this.recipientType=RecipientType.U;
	}
}
