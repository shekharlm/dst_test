package com.communication.message.content;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.googlecode.objectify.annotation.Ignore;
import com.notification.message.content.ChatJsonNotificationPropertyDTO.ChatSubStatus;


/**
 * The Class ChatActionMessage encapsulates all the information required to be Passed for Chat Status and Activity Messages
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class ChatActionMessageDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2527726793776657598L;

	/**
	 * The Enum ChatStatus.
	 */
	public enum ChatStatus {
        INITREQUEST,

        /** The requested. */
        REQUESTED,
        
        /** The queued. */
        QUEUED, 
        
        /** The connected. */
        CONNECTED,

        CONFERENCED,

        /** The responded. */
        RESPONDED, 
        
        /** The abandoned. */
        ABANDONED,
        
        /** The ended. */
        ENDED, 

		/** The exited. */
		EXITED
    }	
	
	/**
	 * The Enum ChatDirection.
	 */
	public enum ChatDirection {
	        
        	/** The inbound. */
        	INBOUND,
	        
        	/** The outbound. */
        	OUTBOUND
	 }
	public enum ChatType {
        
    	/** The inbound. */
    	ChatOnly,
        
    	/** The outbound. */
    	VisualConnect
 }
 
	/** The id. */
	private String id;

	private String requesterURL;
    private String requesterIP;
    private String requesterEmail;
    private String requesterPhoneNumber;
	
	/** The Status. */
	private ChatStatus Status;
	
	private ChatSubStatus subStatus;

    /** true for internal chats (chats between agents) */
    private boolean Internal;

	/** The Direction. */
	private ChatDirection Direction;

	
	/** The chat time stamp. */
	private String chatTimeStamp;
	
	/** The request location. */
	private String  requestLocation;

    private String pageId;
    
    private String otherMessage;
    
    private ChatType chatType ;
    private String name;
    
    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getChatDuration() {
		return chatDuration;
	}

	public void setChatDuration(Long chatDuration) {
		this.chatDuration = chatDuration;
	}

	public String getRequestorIVR() {
		return requestorIVR;
	}

	public void setRequestorIVR(String requestorIVR) {
		this.requestorIVR = requestorIVR;
	}

	private Long chatConnectStartTime;
    
    private Long queueStartTime;
    
    private Long chatStartTime;
    
    private Long chatDuration;
    private String queueName;
	
    private String requestorIVR;
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 *
	 * @param chatId the new id
	 */
	public void setId(String chatId) {
		this.id = chatId;
	}

	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public ChatStatus getStatus() {
		return Status;
	}
	
	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(ChatStatus status) {
		Status = status;
	}

    public boolean isInternal() {
        return Internal;
    }

    public void setInternal(boolean internal) {
        Internal = internal;
    }

    /**
	 * Gets the direction.
	 *
	 * @return the direction
	 */
	public ChatDirection getDirection() {
		return Direction;
	}
	
	/**
	 * Sets the direction.
	 *
	 * @param direction the new direction
	 */
	public void setDirection(ChatDirection direction) {
		Direction = direction;
	}
	

	
	/**
	 * Gets the chat time stamp.
	 *
	 * @return the chat time stamp
	 */
	public String getChatTimeStamp() {
		return chatTimeStamp;
	}
	
	/**
	 * Sets the chat time stamp.
	 *
	 * @param chatTimeStamp the new chat time stamp
	 */
	public void setChatTimeStamp(String chatTimeStamp) {
		this.chatTimeStamp = chatTimeStamp;
	}
	
	/**
	 * Gets the request location.
	 *
	 * @return the request location
	 */
	public String getRequestLocation() {
		return requestLocation;
	}
	
	/**
	 * Sets the request location.
	 *
	 * @param requestLocation the new request location
	 */
	public void setRequestLocation(String requestLocation) {
		this.requestLocation = requestLocation;
	}

    public String getRequesterURL() {
        return requesterURL;
    }

    public void setRequesterURL(String requesterURL) {
        this.requesterURL = requesterURL;
    }

    public String getRequesterIP() {
        return requesterIP;
    }

    public void setRequesterIP(String requesterIP) {
        this.requesterIP = requesterIP;
    }

    public String getRequesterEmail() {
        return requesterEmail;
    }

    public void setRequesterEmail(String requesterEmail) {
        this.requesterEmail = requesterEmail;
    }

    public String getRequesterPhoneNumber() {
        return requesterPhoneNumber;
    }

    public void setRequesterPhoneNumber(String requesterPhoneNumber) {
        this.requesterPhoneNumber = requesterPhoneNumber;
    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

	public String getOtherMessage() {
		return otherMessage;
	}

	public void setOtherMessage(String otherMessage) {
		this.otherMessage = otherMessage;
	}

	public Long getChatConnectStartTime() {
		return chatConnectStartTime;
	}

	public void setChatConnectStartTime(Long chatConnectStartTime) {
		this.chatConnectStartTime = chatConnectStartTime;
	}

	public Long getQueueStartTime() {
		return queueStartTime;
	}

	public void setQueueStartTime(Long queueStartTime) {
		this.queueStartTime = queueStartTime;
	}

	public Long getChatStartTime() {
		return chatStartTime;
	}

	public void setChatStartTime(Long chatStartTime) {
		this.chatStartTime = chatStartTime;
	}

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public ChatType getChatType() {
		return chatType;
	}

	public void setChatType(ChatType chatType) {
		this.chatType = chatType;
	}

	public ChatSubStatus getSubStatus() {
		return subStatus;
	}

	public void setSubStatus(ChatSubStatus subStatus) {
		this.subStatus = subStatus;
	}

}
