package com.communication.message;

import java.io.Serializable;
import java.util.Date;

public class TimestampDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4517897445481323871L;
	private String sid;
	private Date timestamp;
	private Date timestamp2;
	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	public Date getTimestamp2() {
		return timestamp2;
	}
	public void setTimestamp2(Date timestamp2) {
		this.timestamp2 = timestamp2;
	}
	
	
}
