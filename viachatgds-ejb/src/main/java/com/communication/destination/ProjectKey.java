package com.communication.destination;

import com.googlecode.objectify.annotation.Index;

 // TODO: Auto-generated Javadoc
/**
  * The Enum ProjectKey.
  *
  * @author shirshendu
  */
@Index
public enum ProjectKey {
	
	/** The idine. */
	IDINE, 
 /** The awgp. */
 AWGP, 
 /** The onechat. */
 ONECHAT;
}
